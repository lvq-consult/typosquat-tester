const { exists } = require('fs-extra');
const { createReadStream, lstat } = require('fs');
const glob = require('glob');
const { createHash } = require('crypto');
const { SingleBar, Presets } = require('cli-progress');

const REAL_PATH = process.argv[2] || 'node_modules/lodash';
const FAKE_PATH = process.argv[3] || 'node_modules/loadsh';

console.log(`Checking ${REAL_PATH} against ${FAKE_PATH}`);

/**
 *
 * @param {string} path
 * @returns {Promise}
 */
async function fileExists(path) {
  return new Promise(resolve => exists(path, result => resolve(result)));
}

/**
 *
 * @param {string} path
 */
async function isFile(path) {
  return new Promise((resolve, reject) =>
    lstat(path, (err, stats) =>
      err != null ? reject(err) : resolve(stats.isFile()),
    ),
  );
}

/**
 *
 * @param {string} path
 * @returns {Promise}
 */
async function calculateFileHash(path) {
  return new Promise(resolve => {
    const stream = createReadStream(path);
    const hash = createHash('sha256');
    hash.setEncoding('hex');

    stream.on('end', () => {
      hash.end();

      const result = hash.read();

      resolve(result);
    });

    // read all file and pipe it (write it) to the hash object
    stream.pipe(hash);
  });
}

/**
 *
 * @param {string} filename
 * @returns {Promise}
 */
async function checkFile(realfile) {
  const isFileResult = await isFile(realfile);

  if (!isFileResult) {
    return Promise.resolve();
  }

  const fakefile = realfile.replace(REAL_PATH, FAKE_PATH);

  const fakefileExists = await fileExists(fakefile);

  if (!fakefileExists) {
    return Promise.reject(`[does not exists]: ${fakefile}`);
  }

  const realfileHash = await calculateFileHash(realfile);
  const fakefileHash = await calculateFileHash(fakefile);

  if (realfileHash !== fakefileHash) {
    return Promise.reject(`[does not match]: ${realfile} -> ${fakefile}`);
  }

  return Promise.resolve();
}

/**
 * @param {string} pattern
 * @returns {Promise<string[]>}
 */
async function listFiles(pattern) {
  return new Promise((resolve, reject) =>
    glob(pattern, { nodir: true }, (err, matches) =>
      err != null ? reject(err) : resolve(matches),
    ),
  );
}

async function compareFiles() {
  const files1 = await listFiles(`${REAL_PATH}/**/*`);
  const files2 = await listFiles(`${FAKE_PATH}/**/*`);
  const notInFiles2 = files1
    .map(file => file.replace(REAL_PATH, FAKE_PATH))
    .filter(file => files2.includes(file) === false)
    .map(file => file.replace(FAKE_PATH, REAL_PATH));
  const notInFiles1 = files2
    .map(file => file.replace(FAKE_PATH, REAL_PATH))
    .filter(file => files1.includes(file) === false)
    .map(file => file.replace(REAL_PATH, FAKE_PATH));

  if (notInFiles2.length) {
    console.log(`Real files missing in ${FAKE_PATH}`);
    console.log(notInFiles2);
  }

  if (notInFiles1.length) {
    console.log(`Fake files missing in ${REAL_PATH}`);
    console.log(notInFiles1);
  }
}

compareFiles()
  .then(() =>
    listFiles(`${REAL_PATH}/**/*`).then(async files => {
      const results = [];
      const progress = new SingleBar({}, Presets.shades_classic);

      console.log(`Checking ${files.length} files`);
      progress.start(files.length, 0);

      for (let i = 0; i < files.length; i++) {
        try {
          await checkFile(files[i]);
        } catch (err) {
          results.push(err);
        }
        progress.update(i + 1);
      }

      progress.stop();

      if (results.length) {
        console.log('Results');

        results.forEach(result => console.log(result));
      } else {
        console.log('Every file checked matched!');
      }
    }),
  )
  .catch(err => console.error(err));
