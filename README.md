# Typosquat Tester

Tool for checking two npm packages against each other.

It's a simple process:

1. Checks all filenames in both packages against each other, and finds which are missing in the respective folders.
2. Files in both folders are SHA-256 hashed and checked against each other.
3. The tool will report files with mismatching hashes.

## Usage

`typosquat-tester <real_path> <fake_path>`

So one could make a test repo.

1. Install typosquatted package and real package.
   - `npm install --save lodash@4.17.11 loadsh@0.0.4`
2. Install tool
   - `npm install --save typosquat-tester`
3. Run your test
   - `node node_modules/.bin/typosquat-tester node_modules/lodash node_modules/loadsh`
